package Task05;

import java.util.Arrays;
import java.util.List;
import java.util.OptionalInt;

public class Game_03 {
	
	public static void main(String[] args) {
		List<Association> table = Arrays.asList(
				
				new Association(1,"Sri lanka", 35, 10, 8, 9, 855, 520, 341, 57, 21, 8, 7, 80),
				new Association(2,"Australia", 58, 19, 2, 6, 745, 526, 210, 88, 41, 9, 3, 57),
				new Association(3,"New Zealand", 46, 17, 3, 8, 512, 402, 31, 24, 91, 5, 6, 71),
				new Association(4,"West Indies", 16, 11, 4, 47, 574, 355, 556, 51, 35, 7, 6, 44),
				new Association(5,"England", 23, 44, 4, 6, 564, 545, 338, 69, 44, 5, 7, 81),
				new Association(6,"South Africa", 24, 15, 5, 12, 689, 541, 158, 79, 58, 12, 6, 70),
				new Association(7,"Canada", 21, 15, 4, 15, 499, 488, 17, 68, 56, 7, 6, 65),
				new Association(8,"Bangladesh", 24, 12, 2, 14, 557, 523, -46, 66, 57, 7, 7, 53),
				new Association(9,"Zimbabwe", 54, 11, 3, 24, 453, 678, -31, 61, 74, 8, 9, 54),
				new Association(10,"Kenya", 28, 9, 3, 18, 331, 689, -232, 31, 48, 6, 9, 35),
				new Association(11,"Pakistan", 26, 7, 3, 18, 544, 601, -81, 68, 56, 6, 8, 46),
				new Association(12,"Netherlands", 34, 1, 1, 834, 333, 998, -801, 32, 531, 6, 1, 2)
				);
		
		OptionalInt min = table.stream().mapToInt(Association::getPoints).min();
		if(min.isPresent()) {
			System.out.printf("Minimum number of Points is %d\n", min.getAsInt());
		}
		else {
			System.out.println("min failed");
		}
	}

}
