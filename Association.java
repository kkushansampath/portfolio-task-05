package Task05;

public class Association implements Comparable<Association> {
	private int position;
	private String general;
	private int played;
	private int won;
	private int drawn;
	private int lost;
	private int pointsFor;
	private int pointsAgainst;
	private int pointsDifference;
	private int triesFor;
	private int triesAgainst;
	private int tryBouns;
	private int losingBouns;
	private int points;

	public Association(int position, String general, int played, int won, int drawn, 
			int lost, int pointsFor, int pointsAgainst, int pointsDifference, int triesFor,
			int triesAgainst, int tryBouns, int losingBouns, int points) {
		
		this.position = position;
		this.general = general;
		this.played = played;
		this.won = won;
		this.drawn = drawn;
		this.lost = lost;
		this.pointsFor = pointsFor;
		this.pointsAgainst = pointsAgainst;
		this.pointsDifference = pointsDifference;
		this.triesFor = triesFor;
		this.triesAgainst = triesAgainst;
		this.tryBouns = tryBouns;
		this.losingBouns = losingBouns;
		this.points = points;
	}
	
	public String toString() {
		return String.format("%-3d%-20s%10d%10d%10d", position, general, pointsFor,
				pointsAgainst, points);
	}
	
	public int getPosition() {
		return position;
	}
	
	public void setPosition(int position) {
		this.position = position;
	}
	
	public String getGeneral() {
		return general;
	}
	
	public void setGeneral(String general) {
		this.general = general;
	}
	
	public int getPlayed() {
		return played;
	}
	
	public void setPlayed(int played) {
		this.played = played;
	}

	public int getWon() {
		return won;
	}
	
	public void setWon(int won) {
		this.won = won;
	}
	
	public int getDrawn() {
		return drawn;
	}
	
	public void setDrawn(int drawn) {
		this.drawn = drawn;
	}
	
	public int getLost() {
		return lost;
	}
	
	public void setLost(int lost) {
		this.lost = lost;
	}
	
	public int getPoints() {
		return points;
	}
	
	public void setPoints(int points) {
		this.points = points;
	}
	
	public int getPointsFor() {
		return pointsFor;
	}
	
	public void setPointsFor(int pointsFor) {
		this.pointsFor = pointsFor;
	}
	
	public int getPointsAgainst() {
		return pointsAgainst;
	}
	
	public void setPointsAgainst(int pointsAgainst) {
		this.pointsAgainst = pointsAgainst;
	}
	
	public int getPointsDifference() {
		return pointsDifference;
	}
	
	public void setPointDifference(int pointsDifference) {
		this.pointsDifference = pointsDifference;
	}
	
	public int getTriesFor() {
		return triesFor;
	}
	
	public void setTriesFor(int triesFor) {
		this.triesFor = triesFor;
	}
	
	public int getTriesAgainst() {
		return triesAgainst;
	}
	
	public void setTriesAgainst(int triesAgainst) {
		this.triesAgainst = triesAgainst;
	}
	
	public int getTryBouns() {
		return tryBouns;
	}
	
	public void setTryBouns(int tryBouns) {
		this.tryBouns = tryBouns;
	}
	
	public int getlosingBouns() {
		return losingBouns;
	}
	
	public void setlosingBouns(int losingBouns) {
		this.losingBouns = losingBouns;
	}
		
	@Override
	public int compareTo(Association c) {
		// TODO Auto-generated method stub
		return ((Integer) pointsFor).compareTo(c.pointsFor);
	}
	
}
